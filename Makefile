# I use Makefiles to compliment my workflow in Vim. This one automates
# re-creating the venv whenever the dependencies change.

# =====CONFIG=====
# File to run when make run is called
MAIN=main.py

# Source directory
SRC=src

# Directory name of the venv
# Don't put spaces in the VENV name, make does not like spaces
# Run make clean first if you do this after already having created a venv
VENV=venv

# Docs directory
DOCS=docs

# Tests directory
TESTS=tests

# Interpreter to create venv with
INTERPRETER=python3


all: run 

# Re-create venv when needed
$(VENV)/bin/activate: requirements.txt
	@ echo "Rebuilding venv..."
	@ [ ! -e "$(VENV)" ] || rm -rf "$(VENV)"
	@ "$(INTERPRETER)" -m venv "$(VENV)"
	@ "$(VENV)/bin/pip" install -r requirements.txt

# Run script
run: $(VENV)/bin/activate
	@ "$(VENV)/bin/python" "$(MAIN)" 


# =====CLEANING=====
clean: clean-venv clean-cache clean-docs

# Remove venv
clean-venv:
	@ echo "Removing venv..."
	@ [ ! -e "$(VENV)" ] || rm -rf "$(VENV)"

# Remove cache
clean-cache:
	@ echo "Removing .pyc files..."
	@ find . -type f -name "*.pyc" -delete
	@ echo "Removing caches..."
	@ find . -type d \( -name "__pycache__" -o -name ".pytest_cache" \) -exec rm -r "{}" +

clean-docs:
	@ echo "Removing documentation build..."
	@ [ ! -e "$(DOCS)/build" ] || rm -r "$(DOCS)/build"


# =====DOCS=====
$(VENV)/bin/sphinx-build: $(VENV)/bin/activate
	@ echo "Installing sphinx..."
	@ "$(VENV)/bin/pip" install --quiet sphinx

docs: $(VENV)/bin/sphinx-build
	@ "$(VENV)/bin/sphinx-apidoc" -o "$(DOCS)/source" "$(SRC)"
	@ "$(VENV)/bin/sphinx-build" "$(DOCS)/source" "$(DOCS)/build"


# =====TESTS=====
$(VENV)/bin/pytest: $(VENV)/bin/activate
	@ echo "Installing pytest..."
	@ "$(VENV)/bin/pip" install --quiet pytest

test: pytest.ini $(VENV)/bin/pytest
	@ "$(VENV)/bin/pytest" --color=yes


# =====PACKAGING=====
package: README.md LICENSE setup.py test
	@ echo "Removing build..."
	@ [ ! -e "dist" ] || rm -r "dist"
	@ echo "Updating wheel & setuptools..."
	@  "$(VENV)/bin/pip" install --upgrade --quiet setuptools wheel
	@ echo "Running setup.py..."
	@ "$(VENV)/bin/python" setup.py sdist bdist_wheel

# Publish will also come here someday

.PHONY: all run clean clean-venv clean-cache clean-docs test package docs
